angular
	.module('Bread', ['ngRoute', 'ngAnimate'])
	.config(['$routeProvider', '$locationProvider',
	function ($routeProvider, $locationProvider) {
		$routeProvider.
		when('/home', {
			templateUrl: 'templates/home.html',
			controller: 'HomeController',
			controllerAs: 'Home'
		}).
		when('/list', {
			templateUrl: 'templates/list.html',
			controller: 'ListController',
			controllerAs: 'List'
		}).
		when('/login', {
			templateUrl: 'templates/login.html',
			controller: 'LoginController',
			controllerAs: 'Login'
		}).
		otherwise({
			redirectTo: '/login'
		});
	}]).run(['$rootScope','$location', '$routeParams', function($rootScope, $location, $routeParams) {
    $rootScope.$on('$routeChangeSuccess', function(e, current, pre) {
    	$rootScope.currentRoute = $location.path();
    });
  }]);;

angular
	.module('Bread').controller('MainCtrl', function($scope) {
  $scope.name = 'World';
  $scope.showMenu = false;
  $scope.handleMenu = function(){
  	$scope.showMenu = !$scope.showMenu;
  };
});

angular
	.module('Bread').controller('ListController', function($scope) {
  $scope.list = 'World';
});
	angular
	.module('Bread').controller('HomeController', function($scope) {
  $scope.list = 'World';
});
angular
.module('Bread').controller('LoginController', function($scope) {
	$scope.list = 'World';
});


